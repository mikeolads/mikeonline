<!DOCTYPE html>
<html lang="en">

  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="noindex,nofollow" />

    <title>@yield('title', 'Mike Oladapo')</title>

    <!-- Styles -->
    <link href="{{ asset('assets/css/page.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/home.css') }}" rel="stylesheet">

  </head>

  <body>


    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark" data-navbar="static">
	  <div class="container">

	    <div class="navbar-left">
	      <button class="navbar-toggler" type="button">☰</button>
	      <a class="navbar-brand" href="#">
	        <img class="logo-dark" src="../assets/img/logo-dark.png" alt="logo">
	        <img class="logo-light" src="../assets/img/logo-light.png" alt="logo">
	      </a>
	    </div>

	    <section class="navbar-mobile">

	      <span class="navbar-divider d-mobile-none"></span>

	      <ul class="nav nav-navbar mr-auto">
	        <li class="nav-item">
	          <a class="nav-link {{ Route::is('admin.index') ? 'active' : '' }}" href="{{ route('admin.index') }}">Home</a>
	        </li>

	        <li class="nav-item">
	          <a class="nav-link {{ Route::is('admin.projects') ? 'active' : '' }}" href="{{ route('admin.projects') }}">Projects</a>
	        </li>

	        <li class="nav-item">
	          <a class="nav-link {{ Route::is('admin.subscribers') ? 'active' : '' }}" href="{{ route('admin.subscribers') }}">Subscribers</a>
	        </li>

	        <li class="nav-item">
	          <a class="nav-link {{ Route::is('admin.pages') ? 'active' : '' }}" href="{{ route('admin.pages') }}">Pages</a>
	        </li>

            <li class="nav-item">
              <a class="nav-link {{ Route::is('tags.index') ? 'active' : '' }}" href="{{ route('tags.index') }}">Tags</a>
            </li>

	        <li class="nav-item">
	          <a class="nav-link {{ Route::is('admin.testimonials') ? 'active' : '' }}" href="{{ route('admin.testimonials') }}">Testimonials</a>
	        </li>

	      </ul>
	    </section>

	  </div>
	</nav><!-- /.navbar -->


    @yield ('content')

    {{-- POP UP ALERT --}}

    {{-- END POP UP ALERT --}}


    @include ('layouts.partials.script')

    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>

    <script>

        $(document).ready(function(){

            $('#tags').select2();

        })
    </script>

    @if(session('success'))

    	<div id="popup-alert" class="popup col-6 col-md-4" data-position="bottom-left" data-animation="slide-right" data-autohide="3000">

    	  <button type="button" class="close" data-dismiss="popup" aria-label="Close">
    	    <span aria-hidden="true">&times;</span>
    	  </button>

    	  <div class="media">
    	    <div class="media-body">
    	      <h5>SUCCESS!</h5>
    	      <p class="mb-0"> {{ session('success') }} </p>
    	    </div>
    	  </div>
    	</div>

    	<button style="visibility: hidden;" id="alertBtn" data-toggle="popup" data-target="#popup-alert"></button>

    	<script type="text/javascript">

    		addEventListener("load", function(){

    			document.getElementById('alertBtn').click();
    		});

    	</script>

    @endif

  </body>

</html>
