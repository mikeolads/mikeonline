<!-- Footer -->
  <footer class="footer">
    <div class="container text-center">

      <div class="social social-sm social-bg-brand social-cycling">
        <a class="social-facebook" href="{{ $admin->facebook }}" target="_blank"><i class="fa fa-facebook"></i></a>
        <a class="social-twitter" href="{{ $admin->twitter }}" target="_blank"><i class="fa fa-twitter"></i></a>
        <a class="social-instagram" href="{{ $admin->instagram }}" target="_blank"><i class="fa fa-instagram"></i></a>
        <a class="social-instagram" href="{{ $admin->linkedin }}" target="_blank"><i class="fa fa-linkedin"></i></a>
      </div>

      <br>

      <p class="small">Copyright © {{ date('Y') }} <a href="{{ url('/') }}">{{ config('app.name') }}</a>, All rights reserved.</p>

    </div>
  </footer><!-- /.footer -->