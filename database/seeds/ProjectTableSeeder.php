<?php

use Illuminate\Database\Seeder;

use App\Project;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::truncate();
        
        $proj = new Project;
        $proj->title = 'Epigrams';
        $proj->client = 'Epigrams Business Network';
        $proj->url = 'https://epigrams.net';
        $proj->description = 'Fashion/Lifestyle Blog';
        $proj->date = '2017-06-01';
        $proj->avatar = 'assets/img/projects/candace/candace-avatar.png';
        $proj->image = 'assets/img/projects/candace/candace.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();

        // 

        $proj = new Project;
        $proj->title = 'GermaneToMinds';
        $proj->client = 'Elijah Germane';
        $proj->url = 'http://germanetominds.com';
        $proj->description = 'Art and Poetry Service';
        $proj->date = '2016-12-05';
        $proj->avatar = 'assets/img/projects/germane/germane-avatar.png';
        $proj->image = 'assets/img/projects/germane/germane.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();

        // 

        $proj = new Project;
        $proj->title = 'Derins\' Faces';
        $proj->client = 'Aderinsola Titilope';
        $proj->url = 'http://derinsfaces.com.ng';
        $proj->description = 'Makeup/Beauty Agency';
        $proj->date = '2018-01-23';
        $proj->avatar = 'assets/img/projects/derin/derin-avatar.png';
        $proj->image = 'assets/img/projects/derin/derin.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();

        // 

        $proj = new Project;
        $proj->title = 'Jasmine Legacy';
        $proj->client = 'Chidinma Ogbusimba';
        $proj->url = 'http://jasminelegacy.com.ng';
        $proj->description = 'Books and Beauty Store';
        $proj->date = '2018-05-07';
        $proj->avatar = 'assets/img/projects/jasmine/jasmine-avatar.png';
        $proj->image = 'assets/img/projects/jasmine/jasmine.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();

        // 

        $proj = new Project;
        $proj->title = 'Pan African International Magazine';
        $proj->client = 'Pan Africa International';
        $proj->url = 'http://panafricaninternational.com';
        $proj->description = 'Viral News, Magazine and African Awards Platform';
        $proj->date = '2017-02-04';
        $proj->avatar = 'assets/img/projects/pan/pan-avatar.png';
        $proj->image = 'assets/img/projects/pan/pan.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();

        // 

        $proj = new Project;
        $proj->title = 'Super Intelligence Masterclass';
        $proj->client = 'Super Intelligence';
        $proj->url = 'http://superintelligenceng.org';
        $proj->description = 'Consulting/ICT firm.';
        $proj->date = '2018-06-01';
        $proj->avatar = 'assets/img/projects/sim/sim-avatar.png';
        $proj->image = 'assets/img/projects/sim/sim.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();

        // 

        $proj = new Project;
        $proj->title = 'Tomieni Enterprise';
        $proj->client = 'Jesutomi Eniola';
        $proj->url = 'http://tomieni.com.ng';
        $proj->description = 'Fashion Store';
        $proj->date = '2017-12-12';
        $proj->avatar = 'assets/img/projects/tomieni/tomieni-avatar.png';
        $proj->image = 'assets/img/projects/tomieni/tomieni.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();

        // 

        $proj = new Project;
        $proj->title = 'The International Youth Forum';
        $proj->client = 'IYC';
        $proj->url = 'http://theinternationalyouthforum.org';
        $proj->description = 'Non-Governmental Organization';
        $proj->date = '2018-03-12';
        $proj->avatar = 'assets/img/projects/youthforum/youthforum-avatar.png';
        $proj->image = 'assets/img/projects/youthforum/youthforum.png';
        $proj->slug = str_slug($proj->title);

        $proj->save();
    }
}
