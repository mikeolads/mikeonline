<?php $__env->startSection('content'); ?>

 <header class="header text-white p-0 overflow-hidden" data-overlay="9">

    <div class="container text-center">

      <div class="row h-100">
        <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

          <h1>Projects</h1>
          <a href="<?php echo e(route('admin.projects.create')); ?>" class="btn btn-glass btn-primary"> Create New </a>

        </div>

      </div>

    </div>
 </header>

 <main class="main-content">

 	<div class="container mt-6">

 		<div class="row">
 			
		 	<div class="col-md-8">
		 		<table class="table table-striped">
			        <thead>
			          <tr>
			            <th>S/N</th>
			            <th>Title</th>
			            <th>Client</th>
			            <th>Options</th>
			          </tr>
			        </thead>
			        <tbody>
			        	<?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			        	<span style="visibility: hidden;"><?php echo e($index++); ?></span>

			          <tr>
			            <th scope="row"><?php echo e($index); ?></th>
			            <td><?php echo e($p->title); ?></td>
			            <td><?php echo e($p->client); ?></td>
			            <td>
			            	<a href=""><i class="fa fa-trash"></i></a> &nbsp; &nbsp;
			            	<a href="<?php echo e(route('admin.projects.show', $p)); ?>"><i class="fa fa-eye"></i></a> &nbsp; &nbsp;
			            	<a href="<?php echo e(route('admin.projects.edit', $p->id)); ?>"><i class="fa fa-pencil"></i></a>
			            </td>
			          </tr>

			          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			        </tbody>
			      </table>
		 	</div>

 		</div>

 	</div>

 </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>