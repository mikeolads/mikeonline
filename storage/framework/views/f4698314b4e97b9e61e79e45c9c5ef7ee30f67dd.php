<?php $__env->startSection('title', $page->title); ?>

<?php $__env->startSection('meta'); ?>

  <?php if(isset($page) && !is_null($page)): ?>

  <meta name="description" content="<?php echo e($page->description); ?>">
  <meta name="keywords" content="<?php echo e(implode(',', $page->allTags())); ?>">

  <meta name="og:url" content="<?php echo e(url('/') . $page->slug); ?>"/>
  <meta name="og:description" content="<?php echo e($page->description); ?>"/>
  <meta name="og:title" content="<?php echo e($page->title); ?>"/>

  <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <!-- Header -->
    <header class="header h-fullscreen">
      <div class="container">
        <div class="row align-items-center h-100">

          <div class="col-md-5 mr-auto">
            <!-- <h1 class="fw-600">Publish your designed things.</h1> -->
            <h1 class="fw-600">Build your <span class="text-warning">Brand.</span><br><span class="text-info">Sell</span> more stuff.</h1>
            <br>
            <p class="lead-2"> <span class="text-primary fw-400">Mike</span> specializes in motivating and integrating small businesses, startups, companies, and institutions with 21st-century technological services, coupled with world-class marketing analysis and creative propositions.</p>
            <br>
            <a class="btn btn-xl btn-round btn-info" href="<?php echo e(route('aboutPage')); ?>">Learn More <i class="fa fa-arrow-right"></i></a>
          </div>


          <div class="col-md-6">
            <img src="./assets/img/vector/18.png" alt="Mike Oladapo">
          </div>

        </div>
      </div>
    </header><!-- /.header -->


    <!-- Main Content -->
    <main class="main-content">

      <div class="container">

        <header class="section-header">
          <h2 class="text-warning fw-600">My Special Addons</h2>
          <p class="lead">Luxuriate these fringe benefits when you work with me!</p>
        </header>


        <div class="row gap-y">
          <div class="col-md-6">
            <div class="media">
              <div class="mr-5">
                <span class="iconbox iconbox-lg bg-pale-primary text-primary"><i class="fa fa-file-text-o"></i></span>
              </div>
              <div class="media-body">
                <h6>Statistical Analysis</h6>
                <p>Analyze your business concepts and strategies to equip you with the techniques for steady growth and business intelligence.</p>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="media">
              <div class="mr-5">
                <span class="iconbox iconbox-lg bg-pale-danger text-danger"><i class="fa fa-bullhorn"></i></span>
              </div>
              <div class="media-body">
                <h6>Search Optimization</h6>
                <p>Trusted strategies and techniques to get more traffic to your website and higher placement in google searches.</p>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="media">
              <div class="mr-5">
                <span class="iconbox iconbox-lg bg-pale-info text-info"><i class="fa fa-code"></i></span>
              </div>
              <div class="media-body">
                <h6>Marketing Analysis</h6>
                <p>Promote and sell your products and services with high conversion marketing tactics and structured deliverability.</p>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="media">
              <div class="mr-5">
                <span class="iconbox iconbox-lg bg-pale-warning text-warning"><i class="fa fa-comments"></i></span>
              </div>
              <div class="media-body">
                <h6>Website Performance</h6>
                <p>Diagnose your websites and analyze its performance; while offering a seamless performance upgrade and quality user experience.</p>
              </div>
            </div>
          </div>
        </div>


      </div>


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Shots
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section bg-gray">
        <div class="container">
          <header class="section-header">
            <small>Portfolio</small>
            <h2 class="fw-600 text-warning">RECENT PROJECTS</h2>
            <hr>
          </header>


          <div class="row gap-y gap-2">

            <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php echo $__env->make('pages.portfolio._single-partial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          </div>


          

        </div>
      </section>


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | CTA
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section">
        <div class="container">
          <div class="row gap-y align-items-center">
            <div class="col-md-6 text-center">
              <img src="./assets/img/vector/13.png" alt="...">
            </div>

            <div class="col-md-6 text-center text-md-left">
              <h2 class="text-info">SPECIAL MINT OF CREATIVITY
              </h2>
              <p class="lead mb-6">Mike provides sharp minded solutions, while crafting stunning interfaces along. <br>

                With scrutinized and painstakingly researched marketing strategies, he's got all you need to establish your business and increase your sales.

              </p>
              <p><a class="btn btn-lg btn-round btn-primary" href="<?php echo e(route('servicePage')); ?>">View My Services</a></p>
            </div>
          </div>
        </div>
      </section>


      <section class="section p-0">
        <div class="container-fluid px-0">

          <header class="section-header">
            <h2 class="text-warning fw-600">Service Overview</h2>
          </header>

          <div class="row no-gap text-center">

            <div class="col-md-6 bg-gray px-5 py-6 p-md-8">
              <p class="iconbox iconbox-xxl bg-white mb-7">
                <i class="icon-lightbulb lead-6"></i>
              </p>
              <h5 class="mb-5">Business Consulting</h5>
              <p>I'll help you develop masterclass strategies and skills to grow your business and to build a community around it.</p>
              <br>
              <a href="<?php echo e(route('servicePage')); ?>#consulting">Read More</a>
            </div>


            <div class="col-md-6 px-5 py-6 p-md-8">
              <p class="iconbox iconbox-xxl mb-7">
                <i class="icon-linegraph lead-6"></i>
              </p>
              <h5 class="mb-5">Digital Marketing & SEO</h5>
              <p>Give your business a head start with reliable marketing strategies that provide consistent lead generation, product/service exposure, and transforms prospects into clients.</p>
              <br>
              <a href="<?php echo e(route('servicePage')); ?>#marketing">Read More</a>
            </div>

            <div class="col-md-6 px-5 py-6 p-md-8">
              <p class="iconbox iconbox-xxl mb-7">
                <i class="icon-laptop lead-6"></i>
              </p>
              <h5 class="mb-5">Web Design and Development</h5>
              <p>Make your website stand out from the crowd through professionally crafted and user-friendly designs, for qualitative online presence and lead generation.</p>
              <br>
              <a href="<?php echo e(route('servicePage')); ?>#development">Read More</a>
            </div>

            <div class="col-md-6 bg-gray px-5 py-6 p-md-8">
              <p class="iconbox iconbox-xxl bg-white mb-7">
                <i class="icon-phone lead-6"></i>
              </p>
              <h5 class="mb-5">Mobile Development</h5>
              <p>
                App Development and Design <br>
                Prototyping & Idea Dump <br>
                Cross Platform Development with
                Unique UI/UX
              </p>
              <br>
              <a href="<?php echo e(route('servicePage')); ?>#mobile-development">Read More</a>
            </div>

          </div>
        </div>
      </section>

      
      <section class="section">
        <div class="container">
          <header class="section-header">
            <h2>What People Say</h2>
          </header>


          <div class="" data-provide="slider" data-dots="true" data-autoplay="false" data-slides-to-show="2">
  
            <?php $__currentLoopData = $testimonials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $t): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <div class="p-5">
              <div class="card shadow-3">
                <div class="card-body px-6">
                  <div class="rating mb-3">
                    <label class="fa fa-star active"></label>
                    <label class="fa fa-star active"></label>
                    <label class="fa fa-star active"></label>
                    <label class="fa fa-star active"></label>
                    <label class="fa fa-star active"></label>
                  </div>

                  <p class="text-quoted mb-5"><?php echo e($t->message); ?></p>
                  <div class="media align-items-center pb-0">
                    <img class="avatar avatar-xs mr-3" src="<?php echo e($t->avatar()); ?>" alt="...">
                    <div class="media-body lh-1">
                      <div class="fw-400 small-1 mb-1"><?php echo e($t->user_name); ?></div>
                      <small class="text-lighter"><?php echo e($t->user_title); ?></small>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          </div>

        </div>
      </section>

      <?php echo $__env->make('partials.subscribe', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


      <section class="section text-center">
        <div class="container">
          <h2 class="mb-6"><strong>Want to know what I'm up to?</strong></h2>
          <p class="lead text-muted">Check out my blog and be updated on latest news, tutorials, and ideologies.</p>
          <hr class="w-5 my-7">
          <a class="btn btn-lg btn-round btn-info" href="#">Visit Blog</a>
        </div>
      </section>



      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Partners
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section bg-gray">
        <div class="container">
          <header class="section-header">
            <h2>Making Dreams Come True</h2>
            <hr>
            <p class="lead">Satisfied Clients</p>
          </header>

          <div class="partner">
            <img src="./assets/img/partner/sim.png" alt="partner 1">
            <img src="./assets/img/partner/phonesswop.png" height="36" width="150" alt="partner 2">
            <img src="./assets/img/partner/jasmine.png" height="36" width="150" alt="partner 3">
            <img src="./assets/img/partner/pan-african.jpg" height="32" width="70" alt="partner 4">
            <img src="./assets/img/partner/tomieni.png"  height="32" width="150" alt="partner 5">
          </div>

        </div>
      </section>


    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>