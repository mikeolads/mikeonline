<section class="section bg-gray">
  <div class="container text-center">
    <header class="section-header">
      <h2>Do you want your business to be successful?</h2>
      <hr>
      <p class="lead">What if you could get weekly tips on problem-solving techniques, mental creativity,
        art of entrepreneurship, branding, and a host of other innovational articles? All for free? <br>
        <h3 class="text-success fw-400">GOOD NEWS!</h3>
        Just fill the form below and its all yours! <br> Free of Charge!
      </p>
    </header>

    <div class="row">
      <div class="col-md-8 col-xl-6 mx-auto">
        <form class="input-round" method="POST" action="{{ route('newsletter.subscribe') }}">
          @csrf
          <div class="form-group input-group-lg">
            <input type="text" required="" name="name" placeholder="Enter Name" class="form-control">
          </div>
          <div class="form-group input-group input-group-lg">
            <input type="email" required="" name="email" class="form-control" placeholder="Enter Email Address">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit">Subscribe Me</button>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>
</section>