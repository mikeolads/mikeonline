<!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark" data-navbar="smart">
      <div class="container">

        <div class="navbar-left mr-4">
          <button class="navbar-toggler" type="button">&#9776;</button>
          <a class="navbar-brand" href="#">
            <img class="logo-dark" src="{{ asset('assets/img/logo.png') }}" alt="logo">
            <img class="logo-light" src="{{ asset('assets/img/logo.png') }}" alt="logo">
          </a>
        </div>

        <section class="navbar-mobile">
          <span class="navbar-divider d-mobile-none"></span>

          <ul class="nav nav-navbar mr-auto">
            <li class="nav-item">
              <a class="nav-link {{ Route::is('homePage') ? 'active' : '' }}" href="{{ route('homePage') }}">Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link {{ Route::is('aboutPage') ? 'active' : '' }}" href="{{ route('aboutPage') }}">About</a>
            </li>

            <li class="nav-item">
              <a class="nav-link {{ Route::is('servicePage') ? 'active' : '' }}" href="{{ route('servicePage') }}">Services</a>
            </li>

            <li class="nav-item">
              <a class="nav-link {{ Route::is('portfolioPage') ? 'active' : '' }}" href="{{ route('portfolioPage') }}">Portfolio</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="">Blog</a>
            </li>
          </ul>

          <div>
            <a class="btn btn-lg btn-outline-info btn-round btn-info" href="{{ route('contactPage') }}">Contact</a>
          </div>
        </section>

      </div>
    </nav><!-- /.navbar -->