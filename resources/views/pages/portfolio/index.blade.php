@extends ('layouts.master')

@section ('title', $page->title)

@section ('meta')

  @if(isset($page) && !is_null($page))

  <meta name="description" content="{{ $page->description }}">
  <meta name="keywords" content="{{ implode(',', $page->allTags()) }}">

  <meta name="og:url" content="{{ url('/') . $page->slug }}"/>
  <meta name="og:description" content="{{ $page->description }}"/>
  <meta name="og:title" content="{{ $page->title }}"/>

  @endif

@stop

@section ('content')


    <!-- Header -->
    <header class="header bg-gray pt-10 pb-01">
      <div class="container text-center">
        <h1 class="display-4">Portfolio</h1>
      </div>
    </header><!-- /.header -->


    <!-- Main Content -->
    <main class="main-content">


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Shots
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section bg-gray">
        <div class="container">
          
          {{-- <header class="section-header">
            <h2 class="fw-600 text-warning">RECENT PROJECTS</h2>
            <hr>
          </header> --}}


          <div class="row gap-y gap-2">

            @foreach ($projects as $p)

              @include ('pages.portfolio._single-partial')

            @endforeach

          </div>


          {{-- <div class="text-center mt-8">
            <a class="btn btn-secondary" href="#">View More</a>
          </div> --}}

        </div>
      </section>

    </main>

    
@stop