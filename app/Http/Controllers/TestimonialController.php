<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Testimonial::all();

        $index = 0;

        return view('admin.testimonials.index', compact('tests', 'index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'user_name' => 'required|string|unique:testimonials',
            'user_avatar' => 'required|image',
            'user_title' => 'required|string',
            'message' => 'required|string'

        ]);

        $t = new Testimonial;
        $t->user_name = $request->user_name;
        $t->user_title = $request->user_title;
        $t->user_avatar = $request->user_avatar->store('images/testimonials/avatar');
        $t->message = $request->message;

        $t->save();

        session()->flash('success', 'Testimonial created');

        return redirect()->route('admin.testimonials');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        return view('admin.testimonials.show', compact('testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('admin.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        $request->validate([

            'user_name' => 'required|string|unique:testimonials,user_name,' . $testimonial->id,
            'user_avatar' => 'nullable|image',
            'user_title' => 'required|string',
            'message' => 'required|string'

        ]);

        $testimonial->user_name = $request->user_name;
        $testimonial->user_title = $request->user_title;
        $testimonial->message = $request->message;

        if ($request->user_avatar != null) {

            $testimonial->user_avatar = $request->user_avatar->store('images/testimonials/avatar');
        }

        $testimonial->update();

        session()->flash('success', 'Testimonial Updated');

        return redirect()->route('admin.testimonials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        //
    }
}
