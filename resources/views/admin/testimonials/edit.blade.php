@extends ('layouts.admin')

@section ('content')

	<header class="header text-white p-0 overflow-hidden" data-overlay="9">

	   <div class="container text-center">

	     <div class="row h-100">
	       <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

	         <h1>Edit Testimonial</h1>

	       </div>

	     </div>

	   </div>
	</header>

	<main class="main-content mt-8">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					
					<form action="{{ route('admin.testimonials.update', $testimonial) }}" method="POST" enctype="multipart/form-data">
						@csrf

						{{ method_field('PUT') }}
						
						<div class="form-group">
			                <label>User Name</label>
			                <input class="form-control{{ $errors->has('user_name') ? ' is-invalid' : '' }}" name="user_name" type="text" placeholder="User Name" value="{{ old('user_name') ?: $testimonial->user_name }}">

			                @if($errors->has('user_name'))
								<span class="invalid-feedback">
									{{ $errors->first('user_name') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>User Title</label>
			                <input class="form-control{{ $errors->has('user_title') ? ' is-invalid' : '' }}" name="user_title" type="text" placeholder="User Title" value="{{ old('user_title')?: $testimonial->user_title }}">

			                @if($errors->has('user_title'))
								<span class="invalid-feedback">
									{{ $errors->first('user_title') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>User Avatar</label>
			                <input class="form-control{{ $errors->has('user_avatar') ? ' is-invalid' : '' }}" name="user_avatar" type="file" placeholder="Avatar" value="{{ old('user_avatar') }}">

			                @if($errors->has('user_avatar'))
								<span class="invalid-feedback">
									{{ $errors->first('user_avatar') }}
								</span>
			                @endif
			                <div style="margin-top: 10px;">
			                	<span class="help">Current Avatar: <img src="{{ $testimonial->avatar() }}" class="avatar"></span>
			                </div>
			            </div>

			            <div class="form-group">
			                <label>Message</label>
			                <textarea name="message" id="" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" rows="5">{{ old('message') ?: $testimonial->message }}</textarea>

			                @if($errors->has('message'))
								<span class="invalid-feedback">
									{{ $errors->first('message') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group text-center mt-8">
			                <input type="submit" value="Update" class="btn btn-round btn-primary">
			            </div>

					</form>

				</div>
			</div>
		</div>

	</main>

@stop
