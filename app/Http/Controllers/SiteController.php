<?php

namespace App\Http\Controllers;

use App\Project;
use App\Page;
use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class SiteController extends Controller
{
    
	public function homePage()
	{
		$page = Page::where('slug', request()->path())->first();

		$projects = Project::all();

		$testimonials = Testimonial::all();
		
		return view('pages.index', compact('projects', 'testimonials', 'page'));
	}

	public function aboutPage()
	{
		$page = Page::where('slug', request()->path())->first();

		return view('pages.about', compact('page'));
	}

	public function servicePage()
	{
		$page = Page::first();
		return view('pages.services', compact('page'));
	}

	public function contactPage()
	{
		$page = Page::first();
		return view('pages.contact', compact('page'));
	}

	public function portfolioPage()
	{
		$page = Page::first();

		$projects = Project::all();

		return view('pages.portfolio.index', compact('projects', 'page'));
	}

	public function singleProject($slug)
	{
		$project = Project::where('slug', $slug)->firstOrFail();

		$page = Page::first();

		return view('pages.portfolio.single-port', compact('project', 'page'));
	}

}
