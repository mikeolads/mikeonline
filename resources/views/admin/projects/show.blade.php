@extends('layouts.admin')

@section ('content')

	<!-- Header -->
    <header class="header text-center pb-0">
      <div class="container">
        <h1 class="display-4">{{ $project->title }}</h1>
      </div>
    </header><!-- /.header -->


    <!-- Main Content -->
    <main class="main-content">


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Project details
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section bb-1">
        <div class="container">

          <div class="row">
            <div class="col-md-8 mb-6 mb-md-0">
              <img src="{{ $project->image }}" alt="project image">
            </div>


            <div class="col-md-4">
              <h5>Project detail</h5>

              <p>{{$project->description}}</p>

              <ul class="project-detail mt-7">
                <li>
                  <strong>Client</strong>
                  <span>{{ $project->client }}</span>
                </li>

                <li>
                  <strong>Date</strong>
                  <span>{{ date('M j, Y', strtotime($project->date)) }}</span>
                </li>

                <li>
                  <strong>Skills</strong>
                  <span>Design, HTML, CSS, Javascript</span>
                </li>

                <li>
                  <strong>Address</strong>
                  <a href="{{ $project->url }}">{{ $project->url }}</a>
                </li>
              </ul>
            </div>
          </div>

        </div>
      </section>


    </main><!-- /.main-content -->

@stop