@extends ('layouts.admin')

@section ('content')

	<header class="header text-white p-0 overflow-hidden" data-overlay="9">

	   <div class="container text-center">

	     <div class="row h-100">
	       <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

	         <h1>
	         	Edit Project
	         	<small>{{ $p->title }}</small>
	         </h1>

	       </div>

	     </div>

	   </div>
	</header>

	<main class="main-content mt-8">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					
					<form action="{{ route('admin.projects.update', $p) }}" method="POST" enctype="multipart/form-data">
						@csrf

						{{ method_field('PUT') }}
						
						<div class="form-group">
			                <label>Title</label>
			                <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" type="text" placeholder="Title" value="{{ old('title') ?: $p->title }}">

			                @if($errors->has('title'))
								<span class="invalid-feedback">
									{{ $errors->first('title') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>Client</label>
			                <input class="form-control{{ $errors->has('client') ? ' is-invalid' : '' }}" name="client" type="text" placeholder="Client" value="{{ old('client') ?: $p->client }}">

			                @if($errors->has('client'))
								<span class="invalid-feedback">
									{{ $errors->first('client') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>Description</label>
			                <textarea name="description" id="" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5">{{ old('description') ?: $p->description }}</textarea>

			                @if($errors->has('description'))
								<span class="invalid-feedback">
									{{ $errors->first('description') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>Date</label>
			                <input class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" type="date" placeholder="{{ 'Format: ' . date('Y-m-d') }}" value="{{ old('date') ?: $p->date }}">

			                @if($errors->has('date'))
								<span class="invalid-feedback">
									{{ $errors->first('date') }}
								</span>
			                @endif

			                <span class="help">Current Date: {{ date('Y-m-d', strtotime($p->date)) }}</span>
			            </div>

			            <div class="form-group">
			                <label>Url</label>
			                <input class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" type="text" placeholder="Url" value="{{ old('url') ?: $p->url }}">

			                @if($errors->has('url'))
								<span class="invalid-feedback">
									{{ $errors->first('url') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>Avatar <small>(800 X 600)</small></label>
			                <input class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}" name="avatar" type="file" placeholder="Avatar" value="{{ old('avatar') ?:$p->avatar }}">

			                @if($errors->has('avatar'))
								<span class="invalid-feedback">
									{{ $errors->first('avatar') }}
								</span>
			                @endif
			                <span class="help">Current Avatar: <img src="{{ $p->avatar }}" height="50" width="50" alt=""></span>
			            </div>

			            <div class="form-group">
			                <label>Image </label>
			                <input class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" type="file" placeholder="Image" value="{{ old('image') ?:$p->image }}">

			                @if($errors->has('image'))
								<span class="invalid-feedback">
									{{ $errors->first('image') }}
								</span>
			                @endif
			                <span class="help">Current Image: <img src="{{ $p->image }}" height="50" width="50" alt=""></span>
			            </div>

			            <div class="form-group text-center mt-8">
			                <input type="submit" value="Update Project" class="btn btn-round btn-primary">
			            </div>

					</form>

				</div>
			</div>
		</div>

	</main>

@stop
