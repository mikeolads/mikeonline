

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="<?php echo e($admin->name()); ?>">
	<meta name="robots" content="index,follow" />
	<meta name="reply-to" content="hello@mikeoladapo.com">
	<meta name="owner" content="Mike Oladapo">
	<meta name="url" content="http://mikeoladapo.com">
	<link rel="canonical" href="https://mikeoladapo.com">	
	
	<meta name="og:site_name" content="Mike Oladapo"/>
	<meta name="fb:page_id" content="401549620344675" />
	<meta name="og:email" content="hello@mikeoladapo.com"/>
	<meta name="og:phone_number" content="+234817 938 5288"/>
	<meta name="og:image" content="<?php echo e(asset($admin->avatar)); ?>"/>
	<meta name="og:type" content="website"/>

	<?php echo $__env->yieldContent('meta'); ?>

	<title><?php echo $__env->yieldContent('title', 'Mike Oladapo'); ?></title>

	<!-- Styles -->
	<link href="<?php echo e(asset('assets/css/page.min.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('assets/css/style.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('assets/css/home.css')); ?>" rel="stylesheet">

	<!-- Favicons -->
	<link rel="apple-touch-icon" href="">
	<link rel="icon" href="">