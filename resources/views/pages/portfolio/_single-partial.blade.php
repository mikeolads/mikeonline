<div class="col-6 col-lg-3">
  <div class="card shadow-1 hover-shadow-6">
    <a class="p-2" href="{{ route('singleProject', $p->slug) }}">
      <img class="card-img-top" src="{{ asset($p->avatar) }}" alt="{{ $p->title }} - Mike Oladapo">
    </a>
    <div class="card-body flexbox">
      <h6 class="mb-0">
        <img class="avatar avatar-xxs mr-1" src="{{ $admin->avatar }}" alt="Mike Oladapo">
        <a class="small" href="{{ route('singleProject', $p->slug) }}">{{ $p->title }}</a>
      </h6>
      {{-- <a class="text-inherit small-2" href="#"><i class="fa fa-heart pr-1 opacity-60 text-danger"></i> 51</a> --}}
    </div>
  </div>
</div>