<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@homePage')->name('homePage');

Route::get('/about', 'SiteController@aboutPage')->name('aboutPage');

Route::get('/services', 'SiteController@servicePage')->name('servicePage');

Route::get('/contact', 'SiteController@contactPage')->name('contactPage');

Route::get('/portfolio', 'SiteController@portfolioPage')->name('portfolioPage');

Route::get('/portfolio/{slug}', 'SiteController@singleProject')->name('singleProject');

// Admin Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('admin', 'AdminController@index')->name('admin.index');
Route::get('admin/projects', 'ProjectController@index')->name('admin.projects');
Route::get('admin/subscribers', 'SubscriberController@index')->name('admin.subscribers');
Route::get('admin/pages', 'PagesController@index')->name('admin.pages');

// Projects Routes
Route::get('admin/projects/create', 'ProjectController@create')->name('admin.projects.create');
Route::post('admin/projects/store', 'ProjectController@store')->name('admin.projects.store');
Route::get('admin/projects/show/{project}', 'ProjectController@show')->name('admin.projects.show');
Route::get('admin/projects/edit/{project}', 'ProjectController@edit')->name('admin.projects.edit');
Route::put('admin/projects/update/{project}', 'ProjectController@update')->name('admin.projects.update');

// Testimonials Routes
Route::get('admin/testimonials', 'TestimonialController@index')->name('admin.testimonials');
Route::get('admin/testimonials/show/{testimonial}', 'TestimonialController@show')->name('admin.testimonials.show');
Route::get('admin/testimonials/edit/{testimonial}', 'TestimonialController@edit')->name('admin.testimonials.edit');
Route::get('admin/testimonials/create', 'TestimonialController@create')->name('admin.testimonials.create');
Route::post('admin/testimonials/store', 'TestimonialController@store')->name('admin.testimonials.store');
Route::put('admin.testimonials/update/{testimonial}', 'TestimonialController@update')->name('admin.testimonials.update');

// Tags Routes
Route::resource('tags', 'TagController');

// Contact Routes
Route::post('contact/message/send', 'MessageController@store')->name('contact.store');

// Newsletter Routes
Route::post('newsletter/subscribe', 'SubscriberController@store')->name('newsletter.subscribe');

// Admin Pages Routes
Route::get('admin/pages/{page}/edit', 'PagesController@edit')->name('admin.pages.edit');
Route::put('admin/pages/{page}/update', 'PagesController@update')->name('admin.pages.update');
Route::post('admin/pages/store', 'PagesController@store')->name('admin.pages.store');
Route::get('admin/pages/create', 'PagesController@create')->name('admin.pages.create');

