@extends ('layouts.master')

@section ('title', $page->title)

@section ('meta')

  @if(isset($page) && !is_null($page))

  <meta name="description" content="{{ $page->description }}">
  <meta name="keywords" content="{{ implode(',', $page->allTags()) }}">

  <meta name="og:url" content="{{ url('/') . $page->slug }}"/>
  <meta name="og:description" content="{{ $page->description }}"/>
  <meta name="og:title" content="{{ $page->title }}"/>

  @endif

@stop

@section ('content')


    <!-- Header -->
      <header class="header text-white h-fullscreen pb-0 overflow-hidden">
        <div class="container text-center">
          <div class="row align-items-center h-100">

            <div class="col-md-8 mx-auto mt-7">
              <img src="{{ $admin->avatar }}" class="about-page-avatar" alt="mikeoladapo.com - Mike Oladapo">
              <p class="lead text-dark fw-600 mt-4">
                {{ $admin->name() }}
              </p>
              <p class="text-dark">
                {{ $admin->about }}
              </p>

              <div class="social social-bg-brand text-center mb-4">
                <a class="social-facebook" href="{{ $admin->facebook }}"><i class="fa fa-facebook"></i></a>
                <a class="social-twitter" href="{{ $admin->twitter }}"><i class="fa fa-twitter"></i></a>
                <a class="social-instagram" href="{{ $admin->instagram }}"><i class="fa fa-instagram"></i></a>
                <a class="social-linkedin" href="{{ $admin->linkedin }}"><i class="fa fa-linkedin"></i></a>
              </div>
              <!-- <a class="btn btn-lg btn-round btn-primary px-7" href="#">Get started</a> -->
            </div>

          </div>
        </div>
      </header><!-- /.header -->

      <!-- Main Content -->
    <main class="main-content">

      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Our Mission
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section bg-gray">
        <div class="container">
          <div class="row gap-y">

            <div class="col-md-4 mr-md-auto">
              <p class="lead-2">
                
                Offering products and services which are essentially the basic prerequisites for innovative business solutions. They span through the nitty-gritty of business marketing, upselling, branding and web development analysis.
              </p>
            </div>

            <div class="col-md-4">
              <h6>Mission</h6>
              <p>
                Our mission is to attract, integrate, retain and motivate small and large businesses within and without our geographical location towards a glaringly successful outcome.
              </p>
            </div>

            <div class="col-md-4">
              <h6>Vision</h6>
              <p>Our vision is to establish our agency as a leading creative and business hub in the continent.</p>
            </div>

          </div>
        </div>
      </section>

      <section class="section text-white p-0" style="background-color: #33323a;">
        <div class="container-wide">
          <div class="row no-gutters">

            <div class="col-md-4 bg-img" style="background-image: url(./assets/img/avatar/mikeoladapo.jpg); min-height: 300px;"></div>

            <div class="col-md-8 p-6 p-md-8">
              <h4>About Mike</h4>
              <p class="lead">He specializes in motivating and integrating small businesses, startups, companies, and institutions with 21st-century technological services, coupled with world-class marketing analysis and creative propositions. </p>
              <p>He is a business enthusiast and one of the finest programmers in the world. He enjoys programming with PHP, Java, Kotlin, Swift, and Python languages. </p>
              <p>He is a natural creative writer, who loves curating business-centric contents and guides for entrepreneurs and professionals alike.</p>
            </div>

          </div>
        </div>
      </section>

      <section class="section">
        <div class="container">
          <header class="section-header">
            <h2>Passion and Innovation Driven</h2>
            <hr>
            <p class="lead">Optimum service delivery synchronized through classic innovational techniques.</p>
          </header>


          <div class="row">
            <div class="col-lg-8 mx-auto">

              <ol class="timeline">
                <li class="timeline-item">
                  <h4>Secure, Reliable and Fast Architecture</h4>
                  <p><img class="rounded shadow-3" src="./assets/img/thumb/architecture.jpeg" alt="Fast Architecture - mikeoladapo.com"></p>
                  
                </li>

                <li class="timeline-item">
                  <h4>Pixel-perfect design and creativity</h4>
                  <p><img class="rounded shadow-3" src="./assets/img/thumb/design.jpeg" alt="Design and Cerativity -mikeoladapo.com"></p>
                </li>

                <li class="timeline-item">
                  <h4>Unique Brand Identity</h4>
                  <p><img class="rounded shadow-3" src="./assets/img/thumb/brand.jpeg" alt="Brand Identity - mikeoladapo.com"></p>
                </li>

                <li class="timeline-item">
                  <h4>Digital Marketing Integration</h4>
                  <p><img class="rounded shadow-3" src="./assets/img/thumb/digital.jpeg" alt="Digital Marketing - mikeoladapo.com"></p>
                </li>

                <li class="timeline-item">
                  <h4>Mobile and Web Development</h4>
                  <p><img class="rounded shadow-3" src="./assets/img/thumb/development.jpeg" alt="Mobile and Web Development - mikeoladapo.com"></p>
                </li>
              </ol>

            </div>
          </div>

        </div>
      </section>


    </main>

@stop