@extends ('layouts.admin')

@section ('content')

	<header class="header text-white p-0 overflow-hidden" data-overlay="9">

	   <div class="container text-center">

	     <div class="row h-100">
	       <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

	         <h1>Create Testimonial</h1>

	       </div>

	     </div>

	   </div>
	</header>

	<main class="main-content mt-8">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					
					<form action="{{ route('admin.testimonials.store') }}" method="POST" enctype="multipart/form-data">
						@csrf
						
						<div class="form-group">
			                <label>User Name</label>
			                <input class="form-control{{ $errors->has('user_name') ? ' is-invalid' : '' }}" name="user_name" type="text" placeholder="User Name" value="{{ old('user_name') }}">

			                @if($errors->has('user_name'))
								<span class="invalid-feedback">
									{{ $errors->first('user_name') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>User Title</label>
			                <input class="form-control{{ $errors->has('user_title') ? ' is-invalid' : '' }}" name="user_title" type="text" placeholder="User Title" value="{{ old('user_title') }}">

			                @if($errors->has('user_title'))
								<span class="invalid-feedback">
									{{ $errors->first('user_title') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>User Avatar</label>
			                <input class="form-control{{ $errors->has('user_avatar') ? ' is-invalid' : '' }}" name="user_avatar" type="file" placeholder="Avatar" value="{{ old('user_avatar') }}">

			                @if($errors->has('user_avatar'))
								<span class="invalid-feedback">
									{{ $errors->first('user_avatar') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>Message</label>
			                <textarea name="message" id="" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" rows="5">{{ old('message') }}</textarea>

			                @if($errors->has('message'))
								<span class="invalid-feedback">
									{{ $errors->first('message') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group text-center mt-8">
			                <input type="submit" value="Add Testimonial" class="btn btn-round btn-primary">
			            </div>

					</form>

				</div>
			</div>
		</div>

	</main>

@stop
