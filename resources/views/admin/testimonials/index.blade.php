@extends ('layouts.admin')

@section ('content')

 <header class="header text-white p-0 overflow-hidden" data-overlay="9">

    <div class="container text-center">

      <div class="row h-100">
        <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

          <h1>Testimonials</h1>
          <a href="{{ route('admin.testimonials.create') }}" class="btn btn-glass btn-primary"> Create New </a>

        </div>

      </div>

    </div>
 </header>

 <main class="main-content">

 	<div class="container mt-6">

 		<div class="row">
 			
		 	<div class="col-md-8">
		 		<table class="table table-striped">
			        <thead>
			          <tr>
			            <th>S/N</th>
			            <th>Avatar</th>
			            <th>User</th>
			            <th>Title</th>
			            <th>Message</th>
			            <th>Options</th>
			          </tr>
			        </thead>
			        <tbody>
			        	@foreach ($tests as $t)
			        	<span style="visibility: hidden;">{{ $index++ }}</span>

			          <tr>
			            <th scope="row">{{ $index }}</th>
			            <td><img src="{{ $t->avatar() }}" class="avatar" alt=""></td>
			            <td>{{ $t->user_name }}</td>
			            <td>{{ $t->user_title }}</td>
			            <td>{{ $t->message }}</td>
			            <td>
			            	<a href=""><i class="fa fa-trash"></i></a> &nbsp; &nbsp;
			            	<a href="{{ route('admin.testimonials.show', $t) }}"><i class="fa fa-eye"></i></a> &nbsp; &nbsp;
			            	<a href="{{ route('admin.testimonials.edit', $t->id) }}"><i class="fa fa-pencil"></i></a>
			            </td>
			          </tr>

			          @endforeach
			        </tbody>
			      </table>
		 	</div>

 		</div>

 	</div>

 </main>

@stop