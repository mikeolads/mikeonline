<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $with= ['tags'];

    protected $fillable = ['name', 'title', 'slug', 'description'];

    public function tags()
    {
    	return $this->belongsToMany(Tag::class, 'page_tag', 'page_id', 'tag_id');
    }

    public function allTags()
    {

    	$tags =	$this->tags->map(function($tag){
				
				return $tag->name;

    	});

    	return $tags->toArray();
    }

    public function tagIds()
    {
    	$tags =	$this->tags->map(function($tag){
				
				return $tag->id;

    	});

    	return $tags->toArray();
    }
}
