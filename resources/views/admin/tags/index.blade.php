@extends ('layouts.admin')

@section ('content')

 <header class="header text-white p-0 overflow-hidden" data-overlay="9">

    <div class="container text-center">

      <div class="row h-100">
        <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

          <h1>Tags</h1>

        </div>

      </div>

    </div>
 </header>

 <main class="main-content">

 	<div class="container mt-6">

 		<div class="row">

 			<div class="col-md-4">
 				<form action="{{ route('tags.store') }}" method="POST">
 					<div class="form-group">

 						<input type="text" placeholder="Create New Tag" class="form-control" name="name" value="{{ old('name') }}">

 						@csrf
 					</div>

 					<input type="submit" value="Create" class="btn">
 				</form>
 			</div>
 			
		 	<div class="col-md-8">
		 		<table class="table table-striped">
			        <thead>
			          <tr>
			            <th>S/N</th>
			            <th>Name</th>
			            <th>Options</th>
			          </tr>
			        </thead>
			        <tbody>
			        	@foreach ($tags as $t)
			        	<span style="visibility: hidden;">{{ $index++ }}</span>

			          <tr>
			            <th scope="row">{{ $index }}</th>
			            <td>{{ $t->name }}</td>
			            <td> 
							<a href="{{ route('tags.destroy', $t) }}"><i class="fa fa-trash"></i></a> &nbsp; &nbsp;
							<a href="{{ route('tags.edit', $t) }}"><i class="fa fa-pencil"></i></a>
			            </td>
			          </tr>

			          @endforeach
			        </tbody>
			      </table>
		 	</div>

 		</div>

 	</div>

 </main>

@stop