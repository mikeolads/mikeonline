<?php $__env->startSection('title', $page->title); ?>

<?php $__env->startSection('meta'); ?>

  <?php if(isset($page) && !is_null($page)): ?>

  <meta name="description" content="<?php echo e($page->description); ?>">
  <meta name="keywords" content="<?php echo e(implode(',', $page->allTags())); ?>">

  <meta name="og:url" content="<?php echo e(url('/') . $page->slug); ?>"/>
  <meta name="og:description" content="<?php echo e($page->description); ?>"/>
  <meta name="og:title" content="<?php echo e($page->title); ?>"/>

  <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <!-- Header -->
    <header class="header bg-gray pt-10 pb-01">
      <div class="container text-center">
        <h1 class="display-4">Portfolio</h1>
      </div>
    </header><!-- /.header -->


    <!-- Main Content -->
    <main class="main-content">


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Shots
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section bg-gray">
        <div class="container">
          
          


          <div class="row gap-y gap-2">

            <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

              <?php echo $__env->make('pages.portfolio._single-partial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          </div>


          

        </div>
      </section>

    </main>

    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>