@extends('layouts.admin')

@section ('content')

    <!-- Main Content -->
    <main class="main-content">


      <section class="section">
        <div class="container">

          <h1 class="display-4 text-center">{{ $testimonial->user_name }}</h1>
          
          <div class="row">
            <div class="col-md-6 offset-md-3">
              
              <div class="p-5">
                <div class="card shadow-3">
                  <div class="card-body px-6">
                    <div class="rating mb-3">
                      <label class="fa fa-star active"></label>
                      <label class="fa fa-star active"></label>
                      <label class="fa fa-star active"></label>
                      <label class="fa fa-star active"></label>
                      <label class="fa fa-star active"></label>
                    </div>

                    <p class="text-quoted mb-5">{{ $testimonial->message }} </p>
                    <div class="media align-items-center pb-0">
                      <img class="avatar avatar-xs mr-3" src="{{ $testimonial->avatar() }}" alt="...">
                      <div class="media-body lh-1">
                        <div class="fw-400 small-1 mb-1">{{ $testimonial->user_name }}</div>
                        <small class="text-lighter">{{ $testimonial->user_title }}</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
      </section>


    </main><!-- /.main-content -->

@stop