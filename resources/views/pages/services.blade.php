@extends ('layouts.master')

@section ('title', $page->title)

@section ('meta')

  @if(isset($page) && !is_null($page))

  <meta name="description" content="{{ $page->description }}">
  <meta name="keywords" content="{{ implode(',', $page->allTags()) }}">

  <meta name="og:url" content="{{ url('/') . $page->slug }}"/>
  <meta name="og:description" content="{{ $page->description }}"/>
  <meta name="og:title" content="{{ $page->title }}"/>

  @endif

@stop

@section ('content')

    <!-- Header -->
    <header class="header bg-gray pt-10 pb-01">
      <div class="container text-center">
        <h1 class="display-4">Services</h1>
        <p class="lead-2 mt-6">Providing quality services to launch your startup or grow your business</p>
      </div>
    </header><!-- /.header -->


    <!-- Main Content -->
    <main class="main-content">


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Services
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section id="consulting" class="section">
        <div class="container">
          <div class="row gap-y align-items-center">

            <div class="col-md-6 text-md-right">
              <p class="small-2 text-uppercase text-lightest fw-500 ls-1">Consulting</p>
              <h3 class="fw-500">Business Consulting</h3>
              <br>
              <p>
                Developing masterclass strategies and skills to grow businesses and to build a community around them. <hr>
                Strategic Consulting <br>
                Business Modeling <br>
                Competitive Advantage <br>
                Operational Audit


              </p>
            </div>

            <div class="col-md-5 mx-auto">
              <img class="rounded-md" src="./assets/img/thumb/consult.jpeg" alt="Business Consulting - mikeoladapo.com">
            </div>

          </div>
        </div>
      </section>


      <section id="marketing" class="section">
        <div class="container">
          <div class="row gap-y align-items-center">

            <div class="col-md-6 text-center text-md-left order-md-2">
              <p class="small-2 text-uppercase text-lightest fw-500 ls-1">Marketing</p>
              <h3 class="fw-500">Digital Marketing & SEO</h3>
              <br>
              <p>
                  Give your business/service a head start with my reliable marketing strategies that provide consistent lead generation, product/service exposure, and transforms prospects into clients.  <hr>  
                  Social Media Marketing <br>
                  Lead Generation <br>
                  Search Engine Optimization <br>
                  Email & Conversion Optimization

              </p>
            </div>

            <div class="col-md-5 mx-auto">
              <img class="rounded-md" src="./assets/img/thumb/seo.jpeg" alt="Digital Marketing - mikeoladapo.com">
            </div>

          </div>
        </div>
      </section>


      <section id="development" class="section">
        <div class="container">
          <div class="row gap-y align-items-center">

            <div class="col-md-6 text-md-right">
              <p class="small-2 text-uppercase text-lightest fw-500 ls-1">Development</p>
              <h3 class="fw-500">Web Design and Development</h3>
              <br>
              <p>
                Make your website stand out from the crowd through professionally crafted and user-friendly designs, for qualitative online presence and lead generation.  <hr>
                Web Maintenance & Servicing <br>
                Responsive and Creative Designs <br>
                E-commerce Development <br>
                Classified / Online Community <br>

              </p>
            </div>

            <div class="col-md-5 mx-auto">
              <img class="rounded-md" src="./assets/img/thumb/web-dev.jpeg" alt="Web Development - mikeoladapo.com">
            </div>

          </div>
        </div>
      </section>


      <section id="mobile-development" class="section">
        <div class="container">
          <div class="row gap-y align-items-center">

            <div class="col-md-6 text-center text-md-left order-md-2">
              <p class="small-2 text-uppercase text-lightest fw-500 ls-1">Development</p>
              <h3 class="fw-500">Mobile Development</h3>
              <br>
              <p>We not only build web apps, we extend your business to the tip of your clients via our extensible mobile development acumen. Creatively transform your business into a digital trend as we establish your application on major platforms, including Android and IOS. <hr>
                App Development and Design <br>
                Prototyping & Idea Dump <br>
                Cross Platform Development <br>
                Unique UI/UX <br>
              </p>
            </div>

            <div class="col-md-5 mx-auto">
              <img class="rounded-md" src="./assets/img/thumb/mobile.jpeg" alt="Mobile Development - mikeoladapo.com">
            </div>

          </div>
        </div>
      </section>

      <section class="section">
        <div class="container">
          <div class="row gap-y align-items-center">

            <div class="col-md-6 text-md-right">
              <p class="small-2 text-uppercase text-lightest fw-500 ls-1">Branding</p>
              <h3 class="fw-500">Branding and Packaging</h3>
              <br>
              <p>
                Your brand is not a logo. Branding is a way of creating a unique identity for your business. Give me the opportunity to transform your brand, mixing it with some amount of creativity.
                <hr>
                Identity Branding <br>
                Business Persona Development <br>
                Value Proposition <br>
                Color Scheme / Logotype 
              </p>
            </div>

            <div class="col-md-5 mx-auto">
              <img class="rounded-md" src="./assets/img/thumb/branding.jpeg" alt="Branding - mikeoladapo.com">
            </div>

          </div>
        </div>
      </section>


    </main><!-- /.main-content -->

@stop