<?php $__env->startSection('content'); ?>

	<header class="header text-white p-0 overflow-hidden" data-overlay="9">

	   <div class="container text-center">

	     <div class="row h-100">
	       <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

	         <h1>Create Page</h1>

	       </div>

	     </div>

	   </div>
	</header>

	<main class="main-content mt-8">
		
		<div class="container">


			<div class="row">
				<div class="col-md-8">
					
					<form action="<?php echo e(route('admin.pages.store')); ?>" method="POST" enctype="multipart/form-data">

						<?php echo csrf_field(); ?>
						
						<div class="form-group">
			                <label>Name</label>
			                <input class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" type="text" placeholder="Name" value="<?php echo e(old('name')); ?>">

			                <?php if($errors->has('name')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('name')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Title</label>
			                <input class="form-control<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>" name="title" type="text" placeholder="Title" value="<?php echo e(old('title')); ?>">

			                <?php if($errors->has('title')): ?>
								<span class="invalid-feedback">

									<?php echo e($errors->first('title')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Slug</label>
			                <input class="form-control<?php echo e($errors->has('slug') ? ' is-invalid' : ''); ?>" name="slug" type="text" placeholder="Slug" value="<?php echo e(old('slug')); ?>">

			                <?php if($errors->has('slug')): ?>
								<span class="invalid-feedback">

									<?php echo e($errors->first('slug')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">

	                        <label>Select Tags</label>

	                        <select class="form-control select-2" id="tags"  name="tags[]" multiple="">

	                          <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

								<option value="<?php echo e($tag->id); ?>"><?php echo e($tag->name); ?></option>

	                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

	                        </select>

	                     </div>

			            <div class="form-group">
			                <label>Description</label>
			                <textarea name="description" id="" class="form-control<?php echo e($errors->has('description') ? ' is-invalid' : ''); ?>" rows="5"><?php echo e(old('description')); ?></textarea>

			                <?php if($errors->has('description')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('description')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group text-center mt-8">
			                <input type="submit" value="Create" class="btn btn-round btn-primary">
			            </div>

					</form>

				</div>

				<div class="col-md-4">
					
					<form action="<?php echo e(route('tags.store')); ?>" method="POST">
						<?php echo csrf_field(); ?>
						<div class="form-group">
							
							<label for="name">Create Tag</label>

							<input type="text" placeholder="Add New Tag" name="name" class="form-control">

						</div>

						<input type="submit" value="Create Tag" class="btn">
					</form>

				</div>
			</div>
		</div>

	</main>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>