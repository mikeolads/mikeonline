<div class="col-6 col-lg-3">
  <div class="card shadow-1 hover-shadow-6">
    <a class="p-2" href="<?php echo e(route('singleProject', $p->slug)); ?>">
      <img class="card-img-top" src="<?php echo e(asset($p->avatar)); ?>" alt="<?php echo e($p->title); ?> - Mike Oladapo">
    </a>
    <div class="card-body flexbox">
      <h6 class="mb-0">
        <img class="avatar avatar-xxs mr-1" src="<?php echo e($admin->avatar); ?>" alt="Mike Oladapo">
        <a class="small" href="<?php echo e(route('singleProject', $p->slug)); ?>"><?php echo e($p->title); ?></a>
      </h6>
      
    </div>
  </div>
</div>