@extends ('layouts.master')

@section ('title', $page->title)

@section ('meta')

  @if(isset($page) && !is_null($page))

  <meta name="description" content="{{ $page->description }}">
  <meta name="keywords" content="{{ implode(',', $page->allTags()) }}">

  <meta name="og:url" content="{{ url('/') . $page->slug }}"/>
  <meta name="og:description" content="{{ $page->description }}"/>
  <meta name="og:title" content="{{ $page->title }}"/>

  @endif

@stop

@section ('content')

	<!-- Header -->
    <header class="header pt-10 pb-0">
      <div class="container text-center">
        <h1 class="display-4">Get In Touch</h1>
        <p class="lead-2 mt-6">Got any question?</p>
      </div>
    </header><!-- /.header -->


    <!-- Main Content -->
    <main class="main-content">


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Contact form
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section">
        <div class="container">

          <div class="row">
            <form class="col-lg-6 mx-auto p-6 bg-gray rounded" action="{{ route('contact.store') }}" method="POST">
              @if(session('success'))
              <div class="alert alert-success">{{ session('success') }}</div>
              @endif

              @csrf

              <div class="form-row">
                <div class="form-group col-md-6">
                  <input class="form-control form-control-lg {{ $errors->has('name') ? 'is-invalid' : '' }} " type="text" name="name" value="{{ old('name') }}" placeholder="Your Name" required="">
                  @if($errors->has('name'))
                    <span class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </span>
                  @endif
                </div>

                <div class="form-group col-md-6">
                  <input class="form-control form-control-lg {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" value="{{ old('email') }}" required="" placeholder="Your Email Address">

                  @if($errors->has('email'))
                    <span class="invalid-feedback">
                      {{ $errors->first('email') }}
                    </span>
                  @endif
                </div>
              </div>


              <div class="form-group">
                <textarea class="form-control form-control-lg {{ $errors->has('message') ? 'is-invalid' : '' }}" rows="4" placeholder="Your Message" required="" name="message">{{ old('message') }}</textarea>
                @if($errors->has('message'))
                    <span class="invalid-feedback">
                      {{ $errors->first('message') }}
                    </span>
                  @endif
              </div>

              <div class="text-center">
                <button class="btn btn-lg btn-primary" type="submit">SEnd</button>
              </div>
            </form>
          </div>

        </div>
      </section>



      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Map
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section text-white bg-dark bg-img" style="background-image: url(../assets/img/thumb/14.jpg)" data-overlay="6">
        <div class="container">
          <div class="row gap-y align-items-center">

            <div class="col-md-5">
              <p class="text-uppercase small opacity-70 fw-600 ls-1">Head Office</p>
              <h5>Lagos, Nigeria</h5>
              <br>
              <p>Plot 30, Kudirat Abiola Way Oregun, Ikeja</p>
              <p>Phone: <a href="phone:+234817 938 5288">+234817 938 5288</a><br><a href="mailto:hello@mikeoladapo.com">Email: hello@mikeoladapo.com</a></p>
              <br>
              <h6>Follow Us</h6>
              <div class="social social-sm social-inverse">
                <a class="social-twitter" href="{{ $admin->twitter }}"><i class="fa fa-twitter"></i></a>
                <a class="social-facebook" href="{{ $admin->facebook }}"><i class="fa fa-facebook"></i></a>
                <a class="social-instagram" href="{{ $admin->instagram }}"><i class="fa fa-instagram"></i></a>
                <a class="social-dribbble" href="{{ $admin->linkedin }}"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>


            <div class="col-md-7">
              <div class="h-400 rounded" data-provide="map" data-lat="44.540" data-lng="-78.556" data-zoom="10" data-marker-lat="44.540" data-marker-lng="-78.556" data-info="&lt;strong&gt;Our office&lt;/strong&gt;&lt;br&gt;3652 Seventh Avenue, Los Angeles, CA" data-style="light"></div>
            </div>

          </div>
        </div>
      </section>

    </main><!-- /.main-content -->

@stop