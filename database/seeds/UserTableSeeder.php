<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::truncate();

        $u = new \App\User;
        $u->firstname = 'Mike';
        $u->lastname = 'Oladapo';
        $u->username = 'mikeolads';
        $u->email = 'mikewilllogin@mikeonline.co';
        $u->avatar = 'assets/img/avatar/fem.jpg';
        $u->admin = true;
        $u->about = 'Mike is a renowned technologist who employs his expertise as a programmer, business analyst, and digital marketer to develop strategies for innovative business solutions and creative propositions.';
        $u->facebook = 'https://www.facebook.com/mikeonline.co/';
        $u->twitter = 'https://twitter.com/mikeonline_co';
        $u->instagram = 'https://www.instagram.com/mikeonline.co/';
        $u->linkedin = 'https://www.linkedin.com/in/mikeoladapo/';
        $u->password = \Hash::make('femi0809//');
        $u->save();
    }
}
