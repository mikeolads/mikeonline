@extends ('layouts.admin')

@section ('content')

	<header class="header text-white p-0 overflow-hidden" data-overlay="9">

	   <div class="container text-center">

	     <div class="row h-100">
	       <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

	         <h1>Create Page</h1>

	       </div>

	     </div>

	   </div>
	</header>

	<main class="main-content mt-8">
		
		<div class="container">


			<div class="row">
				<div class="col-md-8">
					
					<form action="{{ route('admin.pages.store') }}" method="POST" enctype="multipart/form-data">

						@csrf
						
						<div class="form-group">
			                <label>Name</label>
			                <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" type="text" placeholder="Name" value="{{ old('name') }}">

			                @if($errors->has('name'))
								<span class="invalid-feedback">
									{{ $errors->first('name') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>Title</label>
			                <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" type="text" placeholder="Title" value="{{ old('title') }}">

			                @if($errors->has('title'))
								<span class="invalid-feedback">

									{{ $errors->first('title') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">
			                <label>Slug</label>
			                <input class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" type="text" placeholder="Slug" value="{{ old('slug') }}">

			                @if($errors->has('slug'))
								<span class="invalid-feedback">

									{{ $errors->first('slug') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group">

	                        <label>Select Tags</label>

	                        <select class="form-control select-2" id="tags"  name="tags[]" multiple="">

	                          @foreach  ($tags as $tag)

								<option value="{{ $tag->id }}">{{ $tag->name }}</option>

	                          @endforeach

	                        </select>

	                     </div>

			            <div class="form-group">
			                <label>Description</label>
			                <textarea name="description" id="" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="5">{{ old('description') }}</textarea>

			                @if($errors->has('description'))
								<span class="invalid-feedback">
									{{ $errors->first('description') }}
								</span>
			                @endif
			            </div>

			            <div class="form-group text-center mt-8">
			                <input type="submit" value="Create" class="btn btn-round btn-primary">
			            </div>

					</form>

				</div>

				<div class="col-md-4">
					
					<form action="{{ route('tags.store') }}" method="POST">
						@csrf
						<div class="form-group">
							
							<label for="name">Create Tag</label>

							<input type="text" placeholder="Add New Tag" name="name" class="form-control">

						</div>

						<input type="submit" value="Create Tag" class="btn">
					</form>

				</div>
			</div>
		</div>

	</main>

@stop
