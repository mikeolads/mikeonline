
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>Mike Oladapo — Page not found</title>

    <!-- Styles -->
    <link href="<?php echo e(asset('assets/css/page.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/css/style.css')); ?>" rel="stylesheet">
  </head>

  <body class="layout-centered bg-gray">


    <!-- Main Content -->
    <main class="main-content text-center pb-lg-8">
      <div class="container">

        <h1 class="display-1 text-muted mb-7">Page Not Found</h1>
        <p class="lead">Seems you're looking for something that doesn't exist.</p>
        <br>
        <button class="btn btn-secondary w-150 mr-2" type="button" onclick="window.history.back();">Go back</button>
        <a class="btn btn-secondary w-150" href="/">Return Home</a>

      </div>
    </main><!-- /.main-content -->


    <!-- Scripts -->
    <script src="<?php echo e(asset('assets/js/page.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/script.js')); ?>"></script>

  </body>
</html>
