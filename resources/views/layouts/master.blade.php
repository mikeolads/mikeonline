<!DOCTYPE html>
<html lang="en">

  <head>
    
    @include ('layouts.partials.head')

  </head>

  <body>


    @include ('layouts.partials.nav')


    @yield ('content')


    @include ('layouts.partials.footer')


    @include ('layouts.partials.script');

    @if(session('success'))

    	<div id="popup-alert" class="popup col-6 col-md-4 bg-gradient-dark text-white" data-position="bottom-left" data-animation="slide-right" data-autohide="5000">

    	  <button type="button" class="close" data-dismiss="popup" aria-label="Close">
    	    <span aria-hidden="true">&times;</span>
    	  </button>

    	  <div class="media">
    	    <div class="media-body">
    	      <h5>SUCCESS!</h5>
    	      <p class="mb-0"> {{ session('success') }} </p>
    	    </div>
    	  </div>
    	</div>

    	<button style="visibility: hidden;" id="alertBtn" data-toggle="popup" data-target="#popup-alert"></button>

    	<script type="text/javascript">

    		addEventListener("load", function(){

    			document.getElementById('alertBtn').click();
    		});

    	</script>

    @endif

  </body>
</html>
