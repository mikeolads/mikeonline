<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $index = 0;
        return view('admin.projects.index', compact('projects', 'index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $this->validate($request, [

            'title' => 'required|string',
            'description' => 'required|string',
            'url' => 'required|url',
            'client' => 'required|string',
            'avatar' => 'required|image',
            'image' => 'required|image',
            'date' => 'required|date'

        ]);

        $url = url('/') . '/storage/';

        $pathAvatar = $request->avatar->store('images/projects/avatars');

        $pathImage = $request->image->store('images/projects/images');

        $pathAvatar = $url . $pathAvatar;

        $pathImage = $url . $pathImage;

        $proj = new Project;
        $proj->title = $request->title;
        $proj->client = $request->client;
        $proj->url = $request->url;
        $proj->description = $request->description;
        $proj->date = $request->date;
        $proj->avatar = $pathAvatar;
        $proj->image = $pathImage;
        $proj->slug = str_slug($request->title);

        $proj->save();

        session()->flash('success', 'Project Added');

        return redirect()->route('admin.projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return view('admin.projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('admin.projects.edit', ['p' => $project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $req = $this->validate($request, [

            'title' => 'required|string',
            'description' => 'required|string',
            'url' => 'required|url',
            'client' => 'required|string',
            'avatar' => 'nullable|image',
            'image' => 'nullable|image',
            'date' => 'nullable|date'

        ]);

        $url = url('/') . '/storage/';

        $project->title = $request->title;
        $project->client = $request->client;
        $project->url = $request->url;
        $project->description = $request->description;

        if ($request->date != null) {
            $project->date = $request->date;
        }

        if ($request->avatar != null) {

            $pathAvatar = $request->avatar->store('images/projects/avatars');
            $pathAvatar = $url . $pathAvatar;
            $project->avatar = $pathAvatar;
        }

        if ($request->image != null) {

            $pathImage = $request->image->store('images/projects/images');

            $pathImage = $url . $pathImage;

            $project->image = $pathImage;
        }

        $project->update();

        session()->flash('success', 'Project Updated');

        return redirect()->route('admin.projects.show', $project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
