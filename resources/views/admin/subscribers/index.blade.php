@extends ('layouts.admin')

@section ('content')

 <header class="header text-white p-0 overflow-hidden" data-overlay="9">

    <div class="container text-center">

      <div class="row h-100">
        <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

          <h1>Subscribers</h1>

        </div>

      </div>

    </div>
 </header>

 <main class="main-content">

 	<div class="container mt-6">

 		<div class="row">
 			
		 	<div class="col-md-8">
		 		<table class="table table-striped">
			        <thead>
			          <tr>
			            <th>S/N</th>
			            <th>First Name</th>
			            <th>Last Name</th>
			            <th>Email</th>
			          </tr>
			        </thead>
			        <tbody>
			        	@foreach ($subscribers as $s)
			        	<span style="visibility: hidden;">{{ $index++ }}</span>

			          <tr>
			            <th scope="row">{{ $index }}</th>
			            <td>{{ $s->firstname }}</td>
			            <td>{{ $s->lastname }}</td>
			            <td>
			            	{{ $s->email }}
			            </td>
			          </tr>

			          @endforeach
			        </tbody>
			      </table>
		 	</div>

 		</div>

 	</div>

 </main>

@stop