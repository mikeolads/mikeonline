<?php $__env->startSection('content'); ?>

	<header class="header text-white p-0 overflow-hidden" data-overlay="9">

	   <div class="container text-center">

	     <div class="row h-100">
	       <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

	         <h1>Add New Project</h1>

	       </div>

	     </div>

	   </div>
	</header>

	<main class="main-content mt-8">
		
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3">
					
					<form action="<?php echo e(route('admin.projects.store')); ?>" method="POST" enctype="multipart/form-data">
						<?php echo csrf_field(); ?>
						
						<div class="form-group">
			                <label>Title</label>
			                <input class="form-control<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>" name="title" type="text" placeholder="Title" value="<?php echo e(old('title')); ?>">

			                <?php if($errors->has('title')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('title')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Client</label>
			                <input class="form-control<?php echo e($errors->has('client') ? ' is-invalid' : ''); ?>" name="client" type="text" placeholder="Client" value="<?php echo e(old('client')); ?>">

			                <?php if($errors->has('client')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('client')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Description</label>
			                <textarea name="description" id="" class="form-control<?php echo e($errors->has('description') ? ' is-invalid' : ''); ?>" rows="5"><?php echo e(old('description')); ?></textarea>

			                <?php if($errors->has('description')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('description')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Date</label>
			                <input class="form-control<?php echo e($errors->has('date') ? ' is-invalid' : ''); ?>" name="date" type="date" placeholder="<?php echo e('Format: ' . date('Y-m-d')); ?>" value="<?php echo e(old('date')); ?>">

			                <?php if($errors->has('date')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('date')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Url</label>
			                <input class="form-control<?php echo e($errors->has('url') ? ' is-invalid' : ''); ?>" name="url" type="text" placeholder="Url" value="<?php echo e(old('url')); ?>">

			                <?php if($errors->has('url')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('url')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Avatar <small>(800 X 600)</small></label>
			                <input class="form-control<?php echo e($errors->has('avatar') ? ' is-invalid' : ''); ?>" name="avatar" type="file" placeholder="Avatar" value="<?php echo e(old('avatar')); ?>">

			                <?php if($errors->has('avatar')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('avatar')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group">
			                <label>Image </label>
			                <input class="form-control<?php echo e($errors->has('image') ? ' is-invalid' : ''); ?>" name="image" type="file" placeholder="Image" value="<?php echo e(old('image')); ?>">

			                <?php if($errors->has('image')): ?>
								<span class="invalid-feedback">
									<?php echo e($errors->first('image')); ?>

								</span>
			                <?php endif; ?>
			            </div>

			            <div class="form-group text-center mt-8">
			                <input type="submit" value="Add Project" class="btn btn-round btn-primary">
			            </div>

					</form>

				</div>
			</div>
		</div>

	</main>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>