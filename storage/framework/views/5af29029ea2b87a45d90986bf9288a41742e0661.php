<!DOCTYPE html>
<html lang="en">

  <head>
    
    <?php echo $__env->make('layouts.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  </head>

  <body>


    <?php echo $__env->make('layouts.partials.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <?php echo $__env->yieldContent('content'); ?>


    <?php echo $__env->make('layouts.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <?php echo $__env->make('layouts.partials.script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;

    <?php if(session('success')): ?>

    	<div id="popup-alert" class="popup col-6 col-md-4 bg-gradient-dark text-white" data-position="bottom-left" data-animation="slide-right" data-autohide="5000">

    	  <button type="button" class="close" data-dismiss="popup" aria-label="Close">
    	    <span aria-hidden="true">&times;</span>
    	  </button>

    	  <div class="media">
    	    <div class="media-body">
    	      <h5>SUCCESS!</h5>
    	      <p class="mb-0"> <?php echo e(session('success')); ?> </p>
    	    </div>
    	  </div>
    	</div>

    	<button style="visibility: hidden;" id="alertBtn" data-toggle="popup" data-target="#popup-alert"></button>

    	<script type="text/javascript">

    		addEventListener("load", function(){

    			document.getElementById('alertBtn').click();
    		});

    	</script>

    <?php endif; ?>

  </body>
</html>
