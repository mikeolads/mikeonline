<?php $__env->startSection('content'); ?>

	<!-- Header -->
    <header class="header text-center pb-0">
      <div class="container">
        <h1 class="display-4"><?php echo e($project->title); ?></h1>
      </div>
    </header><!-- /.header -->


    <!-- Main Content -->
    <main class="main-content">


      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | Project details
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      !-->
      <section class="section bb-1">
        <div class="container">

          <div class="row">
            <div class="col-md-8 mb-6 mb-md-0">
              <img src="<?php echo e($project->image); ?>" alt="project image">
            </div>


            <div class="col-md-4">
              <h5>Project detail</h5>

              <p><?php echo e($project->description); ?></p>

              <ul class="project-detail mt-7">
                <li>
                  <strong>Client</strong>
                  <span><?php echo e($project->client); ?></span>
                </li>

                <li>
                  <strong>Date</strong>
                  <span><?php echo e(date('M j, Y', strtotime($project->date))); ?></span>
                </li>

                <li>
                  <strong>Skills</strong>
                  <span>Design, HTML, CSS, Javascript</span>
                </li>

                <li>
                  <strong>Address</strong>
                  <a href="<?php echo e($project->url); ?>"><?php echo e($project->url); ?></a>
                </li>
              </ul>
            </div>
          </div>

        </div>
      </section>


    </main><!-- /.main-content -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>