<!-- Footer -->
  <footer class="footer">
    <div class="container text-center">

      <div class="social social-sm social-bg-brand social-cycling">
        <a class="social-facebook" href="<?php echo e($admin->facebook); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
        <a class="social-twitter" href="<?php echo e($admin->twitter); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
        <a class="social-instagram" href="<?php echo e($admin->instagram); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
        <a class="social-instagram" href="<?php echo e($admin->linkedin); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
      </div>

      <br>

      <p class="small">Copyright © <?php echo e(date('Y')); ?> <a href="<?php echo e(url('/')); ?>"><?php echo e(config('app.name')); ?></a>, All rights reserved.</p>

    </div>
  </footer><!-- /.footer -->