<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth')->except('index');
	}

    public function index()
    {

    	if(!auth()->check()){

    		return view('auth.login');
    	}

    	return view('admin.index');

    }

    public function projectsPage()
    {
    	
    }
}
