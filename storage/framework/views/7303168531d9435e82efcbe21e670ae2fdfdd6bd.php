<!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark" data-navbar="smart">
      <div class="container">

        <div class="navbar-left mr-4">
          <button class="navbar-toggler" type="button">&#9776;</button>
          <a class="navbar-brand" href="#">
            <img class="logo-dark" src="<?php echo e(asset('assets/img/logo.png')); ?>" alt="logo">
            <img class="logo-light" src="<?php echo e(asset('assets/img/logo.png')); ?>" alt="logo">
          </a>
        </div>

        <section class="navbar-mobile">
          <span class="navbar-divider d-mobile-none"></span>

          <ul class="nav nav-navbar mr-auto">
            <li class="nav-item">
              <a class="nav-link <?php echo e(Route::is('homePage') ? 'active' : ''); ?>" href="<?php echo e(route('homePage')); ?>">Home</a>
            </li>

            <li class="nav-item">
              <a class="nav-link <?php echo e(Route::is('aboutPage') ? 'active' : ''); ?>" href="<?php echo e(route('aboutPage')); ?>">About</a>
            </li>

            <li class="nav-item">
              <a class="nav-link <?php echo e(Route::is('servicePage') ? 'active' : ''); ?>" href="<?php echo e(route('servicePage')); ?>">Services</a>
            </li>

            <li class="nav-item">
              <a class="nav-link <?php echo e(Route::is('portfolioPage') ? 'active' : ''); ?>" href="<?php echo e(route('portfolioPage')); ?>">Portfolio</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="">Blog</a>
            </li>
          </ul>

          <div>
            <a class="btn btn-lg btn-outline-info btn-round btn-info" href="<?php echo e(route('contactPage')); ?>">Contact</a>
          </div>
        </section>

      </div>
    </nav><!-- /.navbar -->