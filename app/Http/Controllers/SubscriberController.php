<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use Newsletter;

class SubscriberController extends Controller
{
    public function index()
    {
        $subscribers = Subscriber::all();
        
        return view('admin.subscribers.index', compact('subscribers'));
    }

    public function store()
    {
    	request()->validate([

    		'name' => 'required|string',
    		'email' => 'required|email|unique:subscribers'

    	]);

    	$names = explode(' ', request('name'));

    	$s = new Subscriber;
    	$s->firstname = $names[0];
    	$s->lastname = $names[count($names) - 1];
    	$s->email = request('email');

    	Newsletter::subscribe($s->email, ['firstName'=>$s->firstname, 'lastName'=>$s->lastname]);

        $s->save();

        session()->flash('success', 'Successful! Please click the link sent to your email address to confirm your subscription.');

    	return back();
    }
}
