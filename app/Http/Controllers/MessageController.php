<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function store()
    {
    	request()->validate([

    		'name' => 'required|string',

    		'email' => 'required|email',

    		'message' => 'required|string'

    	]);

    	$m = new Message;

    	$m->name = request('name');
    	$m->email = request('email');
    	$m->message = request('message');

    	$m->save();

    	session()->flash('success', 'Message received! I will get back to you soon!');

    	return back();
    }
}
