<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    public function avatar()
    {
    	return asset($this->user_avatar);
    }
}
