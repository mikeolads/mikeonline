@extends ('layouts.admin')

@section ('content')

 <header class="header text-white p-0 overflow-hidden" data-overlay="9">

    <div class="container text-center">

      <div class="row h-100">
        <div class="col-lg-8 mx-auto align-self-center mb-8 pt-10">

          <h1>Pages</h1>

          <a href="{{ route('admin.pages.create') }}" class="btn">Create New Page</a>

        </div>

      </div>

    </div>
 </header>

 <main class="main-content">

 	<div class="container mt-6">

 		<div class="row">
 			
		 	<div class="col-md-8">
		 		<table class="table table-striped">
			        <thead>
			          <tr>
			            <th>S/N</th>
			            <th>Name</th>
			            <th>Title</th>
			            <th>Slug</th>
			            <th>Description</th>
			            <th>Tags</th>
			          </tr>
			        </thead>
			        <tbody>
			        	@foreach ($pages as $p)
			        	<span style="visibility: hidden;">{{ $index++ }}</span>
			          <tr>
			            <th scope="row">{{ $index }}</th>
			            <td>{{ $p->name }}</td>
			            <td>{{ $p->title }}</td>
			            <td>{{ $p->slug }}</td>
			            <td>{{ $p->description }}</td>
			            <td>
			            	{{ implode(', ', $p->allTags()) }}
			            </td>
			            <td> 
							<a href="{{ route('admin.pages.edit', $p) }}"><i class="fa fa-pencil"></i></a>
			            </td>
			          </tr>

			          @endforeach
			        </tbody>
			      </table>
		 	</div>

 		</div>

 	</div>

 </main>

@stop