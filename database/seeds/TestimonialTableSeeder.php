<?php

use Illuminate\Database\Seeder;
use App\Testimonial;

class TestimonialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Testimonial::truncate();

        $t = new Testimonial;
        $t->user_name = 'Jesutomi Eniola';
        $t->user_title = 'MD, TomiEni Coutures';
        $t->user_avatar = 'assets/img/avatar/tomieni.jpg';
        $t->message = 'My sales skyrocketed and business revived after meeting Mike. Clients keep flooding in after the implementations of the components suggested for me.';

        $t->save();

        // 

        $t = new Testimonial;
        $t->user_name = 'Dr. David';
        $t->user_title = 'Editor, Pan African International';
        $t->user_avatar = 'assets/img/avatar/pan.jpg';
        $t->message = 'Great customer support. Well articulated workflow. Excellent delivery of service and amazing followup tips. Thanks for the great work and job well done.';

        $t->save();

        // 

        $t = new Testimonial;
        $t->user_name = 'Titilola Aderinsola';
        $t->user_title = 'CEO, Derins\' Faces';
        $t->user_avatar = 'assets/img/avatar/derin.jpg';
        $t->message = 'Quality and world-class service at an affordable rate. I\'d recommend Mike to any business person/entrepreneur. Thumbs up.';

        $t->save();

        // 

        $t = new Testimonial;
        $t->user_name = 'Faith Ezzy';
        $t->user_title = 'Blogger';
        $t->user_avatar = 'assets/img/avatar/candace.jpg';
        $t->message = 'Fast, quality and affordable service. I wish I had met these guys earlier. Quick and clear instructions for better performance. It couldn\'t have been any better.';

        $t->save();
    }
}
